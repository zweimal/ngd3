- axes:
    x-axis: done,
    y-axis: done
- month range: done
- line chart: behavior like highcharts
- bar chart: behavior like highcharts
- styles
- tooltips
    functionality: done
    style: missing
- milestones
    align labels: done
- goals
- scroll
    functionality: cue points missing, start-end missing
    component: done
    icons: missing
    arrows: missing
