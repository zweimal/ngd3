import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';

import { ChartDataModel, GoalModel, MetricModel, MilestoneModel } from '../model/chart.model';
import { DateUtility } from '../utils/date-utility';

interface RawMetric {
  value: number;
  source: string;
  adheresToGoal: boolean;
  dateTime: string;
}

interface RawGoal {
  value: string;
  start_date: number;
  end_date?: number;
}

@Injectable()
export class ChartService {
  public getChartData(patient): Observable<ChartDataModel> {
    const rawData = this.getRawData();
    const today = DateUtility.today();
    const goals = this._parseGoals(rawData.goals);
    const metrics = this._parseMetrics(rawData.metrics);
    const milestones = this._getMilestonesData(patient, today);

    return Observable.from([{goals, metrics, milestones, today}]);
  }

  private _parseGoals(rawGoals: RawGoal[]): GoalModel[] {
    return rawGoals.map(data => {
      return {
        value: +data.value,
        startDate: DateUtility.convertUnixToLocalDate(data.start_date),
        endDate: DateUtility.convertUnixToLocalDate(data.end_date)
      };
    });
  }

  private _parseMetrics(rawMetrics: RawMetric[]): MetricModel[] {
    rawMetrics = rawMetrics.sort((a, b) => a.dateTime < b.dateTime ? -1 : 1);
    return rawMetrics.map(m => {
      return {
        adheresToGoal: m.adheresToGoal,
        date: DateUtility.resetDay(m.dateTime),
        value: m.value,
      };
    });
  }

  private _getMilestonesData(patient, today: Date): MilestoneModel[] {
    const registrationDate = DateUtility.resetDay(patient.registrationDate);
    const surgeryDate = DateUtility.resetDay(patient.surgeryDate);

    const list = [
      { date: registrationDate, label: 'PATIENT ADDED' },
      { date: today, label: 'TODAY'},
      { date: surgeryDate, label: 'SURGERY'}
    ];
    return list.sort((a, b) => a.date.getTime() - b.date.getTime());
  }

  private getRawData() {
    return {
      compliance: {
          score: 0,
          isCompliant: false,
          log: {
              weight: {
                  score: 0,
                  metrics: {
                      '2018-02-21': {
                          goal: {
                              value: '395.000'
                          },
                          metric: null
                      },
                      '2018-02-20': {
                          goal: {
                              value: '395.000'
                          },
                          metric: null
                      },
                      '2018-02-19': {
                          goal: {
                              value: '395.000'
                          },
                          metric: null
                      },
                      '2018-02-18': {
                          goal: {
                              value: '395.000'
                          },
                          metric: null
                      }
                  }
              }
          }
      },
      goals: [
          {
              key: 'weight',
              value: '395.000',
              unit: 'lbs',
              per: null,
              start_date: 1484438400,
              end_date: null
          }
      ],
      metrics: [
          {
              key: 'weight',
              value: 379,
              source: 'Fitbit',
              adheresToGoal: true,
              dateTime: '2017-10-20T00:00:00-04:00'
          },
          {
              key: 'weight',
              value: 380,
              source: 'Fitbit',
              adheresToGoal: true,
              dateTime: '2017-10-17T00:00:00-04:00'
          },
          {
              key: 'weight',
              value: 385,
              source: 'Fitbit',
              adheresToGoal: true,
              dateTime: '2017-10-15T00:00:00-04:00'
          },
          {
              key: 'weight',
              value: 390,
              source: 'Fitbit',
              adheresToGoal: true,
              dateTime: '2017-10-12T00:00:00-04:00'
          },
          {
              key: 'weight',
              value: 395,
              source: 'Fitbit',
              adheresToGoal: true,
              dateTime: '2017-10-05T00:00:00-04:00'
          },
          {
              key: 'weight',
              value: 400,
              source: 'Fitbit',
              adheresToGoal: false,
              dateTime: '2017-09-30T00:00:00-04:00'
          },
          {
              key: 'weight',
              value: 410,
              source: 'Fitbit',
              adheresToGoal: false,
              dateTime: '2017-09-20T00:00:00-04:00'
          },
          {
              key: 'weight',
              value: 415,
              source: 'Fitbit',
              adheresToGoal: false,
              dateTime: '2017-09-11T00:00:00-04:00'
          }
      ]
    };
  }

}
