
export default function pairwise<T>(arr: T[], func: (a: T, b: T) => void) {
  for (let i = 0; i < arr.length - 1; i++) {
    func(arr[i], arr[i + 1]);
  }
}
