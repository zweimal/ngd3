const ISO_DATE_REGEX = /\d{4}-[01]\d-[0-3]\d([T ][0-2]\d:[0-5]\d:[0-5]\d((\.\d+)?[+-][0-2]\d:[0-5]\d|Z)?)?/;

export type AnyValidDateArg = Date | string | number | number[];

export class DateUtility {

  public static today() {
    return DateUtility.removeHoursFromDay(new Date());
  }

  /**
   * Check if the date input falls on the same day as today
   */
  public static isToday(date: Date) {
    const today = new Date();
    return today.getFullYear() === date.getFullYear() &&
      today.getMonth() === date.getMonth() &&
      today.getDate() === date.getDate();
  }

  public static isGreater(a: Date, b: Date): boolean {
    return a.getTime() > b.getTime();
  }

  public static convertUnixToLocalDate(secs: number): Date {
    if (!secs) {
      return undefined;
    }

    const date = new Date();
    date.setTime((secs + date.getTimezoneOffset() * 60) * 1000);

    return date;
  }

  public static createDate(date: Readonly<AnyValidDateArg>): Date {
    if (date instanceof Array) {
        return new Date(...date);
    } else if (date instanceof Date) {
        return new Date(date.getTime());
    } else if (typeof(date) === 'string' && date.match(ISO_DATE_REGEX)) {

        const [year, month, day] = date.match(/(\d{4})-(\d{2})-(\d{2})/).slice(1);
        const timeMatch = date.match(/(\d{2}):(\d{2}):(\d{2})/);
        const [hours, minutes, seconds] = timeMatch ? timeMatch.slice(1) : [0, 0, 0];

        return new Date(+year, +month - 1, +day, +hours, +minutes, +seconds);
    }

    return new Date(date as any);
  }

  public static removeHoursFromDay(day: Date): Date {
    day.setHours(0, 0, 0, 0);
    return day;
  }

  public static resetDay(date: Readonly<AnyValidDateArg>): Date {
    return DateUtility.removeHoursFromDay(DateUtility.createDate(date));
  }

}
