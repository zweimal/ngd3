import { Component } from '@angular/core';

import 'rxjs/add/operator/take';

import { ChartDataModel } from '../../model/chart.model';
import { ChartService } from '../../services/chart.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {

  public patient = {
    registrationDate: '2018-03-16T13:20:54+00:00',
    surgeryDate: '2018-03-01'
  };
  public chartData: ChartDataModel;

  constructor(private _service: ChartService) {
    this._service.getChartData(this.patient).take(1).subscribe(data => this.chartData = data);
  }

}
