import { Component, OnInit } from '@angular/core';

import * as d3 from 'd3-selection';
import * as d3Array from 'd3-array';
import * as d3Scale from 'd3-scale';
import * as d3Time from 'd3-time';

import { ChartDataModel, MetricModel, MilestoneModel, GoalModel } from '../../model/chart.model';
import { XScale, YScale } from '../../model/chart-view.model';

@Component({
  selector: 'hp-hcp-bariatric-chart-base',
  template: ''
})
export class ChartBaseComponent {

  public width = 1064;
  public height = 243;
  public topMargin = 30;
  public bottomMargin = 60;
  public bottomSpacer = 15;
  public leftMargin = 50;
  public rightMargin = 24;

  public bottomY: number;
  public graphHeight: number;
  public graphWidth: number;
  public xScale: XScale;
  public yScale: YScale;

  protected _calculateGraphSize() {
    this.graphHeight = this.height - this.topMargin - this.bottomMargin;
    this.graphWidth = this.width - this.rightMargin - this.leftMargin;
    this.bottomY = this.topMargin + this.graphHeight + this.bottomSpacer;
  }

  protected _initXScale(milestones: MilestoneModel[]): void {
    const firstDate = d3Time.timeMonth.floor(milestones[0].date);
    const lastDate = d3Time.timeMonth.ceil(milestones[milestones.length - 1].date);
    const days = d3Time.timeDay.count(firstDate, lastDate);

    this.xScale = d3Scale.scaleTime()
      .range([0, days * 11])
      .domain([ firstDate, lastDate ]);
  }

  protected _initYScale(metrics: MetricModel[], goals: GoalModel[], minValue?: number) {
    const data = [...metrics, ...goals];
    let maxValue = d3Array.max(data, (d) => d.value);
    const unit = this._getRoundingUnit(maxValue);
    maxValue += unit - maxValue % unit;
    if (!minValue) {
      minValue = d3Array.min(data, (d) => d.value);
      minValue -= minValue % unit;
    }

    this.yScale = d3Scale.scaleLinear()
      .range([this.graphHeight, 0])
      .domain([minValue, maxValue]);
  }

  private _getRoundingUnit(num: number): number {

    if (num < 100) {
        return 1;
    }
    if (num < 1000) {
        return 10;
    }
    if (num < 10000) {
        return 100;
    }
    if (num < 100000) {
        return 1000;
    }
    if (num < 1000000) {
        return 10000;
    }
    return NaN;
  }

}
