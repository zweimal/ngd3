import { Component, Input, OnChanges, ElementRef } from '@angular/core';

import * as d3Shape from 'd3-shape';

import { GoalModel } from '../../../model/chart.model';
import { XScale, YScale } from '../../../model/chart-view.model';

interface GoalViewModel {
  goal: GoalModel;
  line: string;
  startX: number;
  endX: number;
  y: number;
}

@Component({
  selector: 'svg:g[hp-hcp-bariatric-goals]',
  templateUrl: './goals.html',
  styleUrls: ['./goals.scss']
})
export class GoalsComponent implements OnChanges {

  @Input() public goals: GoalModel[];
  @Input() public unit: string;
  @Input() public xScale: XScale;
  @Input() public yScale: YScale;

  public goalLines: GoalViewModel[];

  constructor(private _element: ElementRef) { }

  public ngOnChanges() {
    if (this.xScale && this.yScale && this.goals) {
        this._processGoals();
    }
  }

  public handleRollOver(event) {
    console.log(event);
    console.log(this._element);
  }

  private _processGoals() {
    const xScale = this.xScale;
    const yScale = this.yScale;
    const domain = xScale.domain();
    const lastDate = domain[domain.length - 1];

    this.goalLines = this.goals.map(goal => {
      const y = Math.round(yScale(goal.value));
      const startX = Math.round(xScale(goal.startDate));
      const endX = Math.round(xScale(goal.endDate || lastDate));

      return {
        goal,
        line: this._createLine(startX, endX, y),
        startX,
        endX,
        y
      };
    });
  }

  private _createLine(startX: number, endX: number, y: number): string {
    const generator = d3Shape.line()
      .x((m => m[0]) )
      .y((m => m[1]) );

    return generator([[startX, y], [endX, y]]);
  }

}
