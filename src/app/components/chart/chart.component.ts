import { Component, Input, OnChanges } from '@angular/core';

import { ChartBaseComponent } from './chart-base.component';
import { GoalModel, MetricModel, MilestoneModel } from '../../model/chart.model';

@Component({
  selector: 'hp-hcp-bariatric-chart',
  templateUrl: './chart.html',
  styleUrls: ['./chart.scss']
})
export class ChartComponent extends ChartBaseComponent implements OnChanges {

  @Input() public goals: GoalModel[];
  @Input() public metrics: MetricModel[];
  @Input() public milestones: MilestoneModel[];
  @Input() public today: Date;
  @Input() public type: 'bars' | 'line' = 'line';
  @Input() public unit = '';

  constructor() {
    super();
    this._calculateGraphSize();
  }

  public ngOnChanges() {
    this._initXScale(this.milestones);
    this._initYScale(this.metrics, this.goals);
  }
}
