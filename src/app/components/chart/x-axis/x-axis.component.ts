import { Component, Input, OnChanges } from '@angular/core';

import * as d3Time from 'd3-time';
import * as d3TimeFormat from 'd3-time-format';

import { XScale } from '../../../model/chart-view.model';
import pairwise from '../../../utils/pairwise';

@Component({
  selector: 'svg:g[hp-hcp-chart-x-axis]',
  templateUrl: './x-axis.html',
  styleUrls: ['./x-axis.scss']
})
export class XAxisComponent implements OnChanges {

  @Input() public xScale: XScale;
  @Input() public height: number;

  public labels: { x: number, text: string }[];
  public ticks: number[];

  public ngOnChanges() {
    const xScale = this.xScale;
    const ticks = xScale.ticks(d3Time.timeMonth);

    this.ticks = ticks.slice(1, -1).map(d => Math.round(xScale(d)));

    const labels = this.labels = [];
    const formatter = d3TimeFormat.timeFormat('%B');

    pairwise(ticks, (a, b) => {
      labels.push({
        x: (xScale(a) + xScale(b)) / 2,
        text: formatter(a).toUpperCase()
      });
    });
  }

}
