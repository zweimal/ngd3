import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

import * as d3 from 'd3-selection';
import * as d3Zoom from 'd3-zoom';

@Component({
  selector: 'svg:g[hp-hcp-bariatric-chart-scroller]',
  templateUrl: './scroller.component.html',
  styleUrls: ['./scroller.component.scss']
})
export class ScrollerComponent implements AfterViewInit {

  @Input() public height: number;
  @Input() public width: number;

  public uuid: string;

  @ViewChild('scrollable') private _scrollable: ElementRef;
  private $scrollable: any;

  constructor(private _host: ElementRef) {
    this.uuid = 'clip' + (Math.random() * 0xffffffff).toString(16);
  }

  public ngAfterViewInit() {
    const zoom = d3Zoom.zoom()
      .scaleExtent([1, 1]) // Only allow translations
      .on('zoom', this.onZoom.bind(this));

    const $host = d3.select(this._host.nativeElement);
    $host.call(zoom);

    this.$scrollable = d3.select(this._scrollable.nativeElement);
  }

  private onZoom(d) {
    const e = d3.event;
    const dx = e.transform.x;
    this.$scrollable.attr('transform', 'translate(' + dx + ',0)');
  }

}
