import { Component, Input, OnChanges } from '@angular/core';
import { YScale } from '../../../model/chart-view.model';

@Component({
  selector: 'svg:g[hp-hcp-chart-y-axis]',
  templateUrl: './y-axis.html',
  styleUrls: ['./y-axis.scss']
})
export class YAxisComponent implements OnChanges {

  @Input() public yScale: YScale;

  public yTicks: {y: number, value: number}[];

  public ngOnChanges() {
    const yScale = this.yScale;
    const ticks = yScale.ticks(5);
    this.yTicks = ticks.map(value => {
      return {
        value,
        y: yScale(value)
      };
    });
  }

}
