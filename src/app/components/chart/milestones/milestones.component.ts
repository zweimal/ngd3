
import { Component, Input, OnChanges } from '@angular/core';

import { DateUtility } from '../../../utils/date-utility';
import { MilestoneModel } from '../../../model/chart.model';
import pairwise from '../../../utils/pairwise';
import { XScale } from '../../../model/chart-view.model';

interface DotModel {
  alignment: 'align-center' | 'align-left' | 'align-right';
  milestone: MilestoneModel;
  offset: number;
  x: number;
}

type Pair<T> = [T, T];

const MILESTONE_AVG_LABEL_WIDTH = 70;
const MILESTONE_OFFSET = 4;

@Component({
  selector: 'svg:g[hp-hcp-bariatric-chart-milestones]',
  templateUrl: './milestones.html',
  styleUrls: ['./milestones.scss']
})
export class MilestonesComponent implements OnChanges {

  @Input() public xScale: XScale;
  @Input() public milestones: MilestoneModel[];

  public dots: DotModel[];
  public lastDotIndex: number;
  public lastX: number;

  public ngOnChanges() {
    const range = this.xScale.range();
    const milestones = this.milestones;

    this._processMilestones();
    this._alignDotLabels();
    this.lastDotIndex = this.dots.length - 1;
    this.lastX = range[range.length - 1];
  }

  private _processMilestones() {
    const xScale = this.xScale;
    this.dots = this.milestones.map<DotModel>(milestone => {
      return {
        alignment: 'align-center',
        milestone,
        offset: 0,
        x: xScale(milestone.date)
      };
    });
  }

  private _alignDotLabels(): void {
    const collisions = this._getDotCollisions();

    // TODO: rare case -> collisions.length > 1
    if (collisions.length === 1) {
        const collision = collisions[0];
        collision[0].alignment = 'align-right';
        collision[0].offset = -MILESTONE_OFFSET;
        collision[1].alignment = 'align-left';
        collision[1].offset = MILESTONE_OFFSET;
    }
}

private _getDotCollisions(): Pair<DotModel>[] {
    const collisions: Pair<DotModel>[] = [];

    pairwise(this.dots, (a, b) => {
      if (Math.abs(a.x - b.x) < MILESTONE_AVG_LABEL_WIDTH) {
        collisions.push([a, b]);
      }
    });

    return collisions;
}


}
