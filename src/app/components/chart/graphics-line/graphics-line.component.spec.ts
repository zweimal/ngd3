import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphicsLineComponent } from './graphics-line.component';

describe('GraphicLineComponent', () => {
  let component: GraphicsLineComponent;
  let fixture: ComponentFixture<GraphicsLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphicsLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphicsLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
