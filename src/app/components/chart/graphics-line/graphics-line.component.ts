import { Component, Input, OnChanges } from '@angular/core';

import * as d3Shape from 'd3-shape';

import { MetricModel } from '../../../model/chart.model';
import { XScale, YScale } from '../../../model/chart-view.model';
import { DateUtility } from '../../../utils/date-utility';

interface EventModel {
  metric: MetricModel;
  title?: string;
  x: number;
  y: number;
}

@Component({
  selector: 'svg:g[hp-hcp-bariatric-graphics-line]',
  templateUrl: './graphics-line.html',
  styleUrls: ['./graphics-line.scss']
})
export class GraphicsLineComponent implements OnChanges {

  @Input() public metrics: MetricModel[];
  @Input() public today: Date;
  @Input() public unit: string;
  @Input() public xScale: XScale;
  @Input() public yScale: YScale;

  public history: EventModel[];
  public line: string;

  public todayEvent?: EventModel;
  public todayLine?: string;

  ngOnChanges() {
    if (this.metrics) {
      this._processHistory();
      this._getTodayEvent();
      this._setLinesData();
    }
  }

  private _processHistory() {
    const xScale = this.xScale;
    const yScale = this.yScale;
    this.history = this.metrics.map(metric => {
      return {
        metric,
        x: Math.round(xScale(metric.date)),
        y: Math.round(yScale(metric.value))
      };
    });
  }

  private _getTodayEvent() {
    const history = this.history;

    const lastEvent = history[history.length - 1];
    if (!lastEvent) {
      return;
    }

    const lastMetric = lastEvent.metric;

    if (!DateUtility.isToday(lastMetric.date)) {
      this.todayEvent = {
        title: 'last reported',
        metric: lastMetric,
        x: this.xScale(this.today),
        y: lastEvent.y
      };
    }
  }

  private _setLinesData() {
    const xScale = this.xScale;
    const generator = d3Shape.line()
      .x( <any> (m => m.x) )
      .y( <any> (m => m.y) );

    const history: any[] = this.history;
    this.line = generator(history);

    if (this.todayEvent) {
      const lastEvent = history[history.length - 1];
      this.todayLine = generator([lastEvent, this.todayEvent]);
    }
  }
}
