import { Component, Input, OnChanges } from '@angular/core';

import * as d3Shape from 'd3-shape';

import { MetricModel } from '../../../model/chart.model';
import { XScale, YScale } from '../../../model/chart-view.model';

@Component({
  selector: 'svg:g[hp-hcp-bariatric-graphics-bars]',
  templateUrl: './graphics-bars.html',
  styleUrls: ['./graphics-bars.scss']
})
export class GraphicsBarsComponent implements OnChanges {

  @Input() public bottomY: number;
  @Input() public metrics: MetricModel[];
  @Input() public unit: string;
  @Input() public xScale: XScale;
  @Input() public yScale: YScale;

  public history: { x: number, y: number, metric: MetricModel }[];

  ngOnChanges() {
    if (this.metrics) {
      this._processHistory();
    }
    if (this.bottomY === undefined) {
      const range = this.yScale.range();
      this.bottomY = range[0];
    }
  }

  private _processHistory() {
    const xScale = this.xScale;
    const yScale = this.yScale;
    this.history = this.metrics.map(metric => {
      return {
        metric,
        x: Math.round(xScale(metric.date)),
        y: Math.round(yScale(metric.value))
      };
    });
  }

}

