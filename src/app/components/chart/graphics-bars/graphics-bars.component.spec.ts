import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphicsBarsComponent } from './graphics-bars.component';

describe('GraphicsBarsComponent', () => {
  let component: GraphicsBarsComponent;
  let fixture: ComponentFixture<GraphicsBarsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphicsBarsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphicsBarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
