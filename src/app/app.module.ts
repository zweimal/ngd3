import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ChartBaseComponent } from './components/chart/chart-base.component';
import { ChartComponent } from './components/chart/chart.component';
import { ChartService } from './services/chart.service';
import { GraphicsBarsComponent } from './components/chart/graphics-bars/graphics-bars.component';
import { GraphicsLineComponent } from './components/chart/graphics-line/graphics-line.component';
import { MilestonesComponent } from './components/chart/milestones/milestones.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { ScrollerComponent } from './components/chart/scroller/scroller.component';
import { XAxisComponent } from './components/chart/x-axis/x-axis.component';
import { YAxisComponent } from './components/chart/y-axis/y-axis.component';
import { GoalsComponent } from './components/chart/goals/goals.component';


@NgModule({
  declarations: [
    AppComponent,
    ChartBaseComponent,
    ChartComponent,
    GraphicsBarsComponent,
    GraphicsLineComponent,
    MilestonesComponent,
    ProfileComponent,
    ScrollerComponent,
    XAxisComponent,
    YAxisComponent,
    GoalsComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    TooltipModule.forRoot()
  ],
  providers: [ ChartService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
