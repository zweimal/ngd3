export interface ChartDataModel {
  goals: GoalModel[];
  metrics: MetricModel[];
  milestones: MilestoneModel[];
  today: Date;
}

export interface GoalModel {
  value: number;
  startDate: Date;
  endDate?: Date;
}

export interface MetricModel {
  adheresToGoal: boolean;
  date: Date;
  value: number;
}

export interface MilestoneModel {
  date: Date;
  label: string;
}
