import { ScaleTime, ScaleLinear } from 'd3';

export type XScale = ScaleTime<number, number>;
export type YScale = ScaleLinear<number, number>;
